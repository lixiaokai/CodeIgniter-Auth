-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.5.24-log - MySQL Community Server (GPL)
-- Server OS:                    Win32
-- HeidiSQL Version:             9.1.0.4868
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table ci_auth.ci_admin_menu
DROP TABLE IF EXISTS `ci_admin_menu`;
CREATE TABLE IF NOT EXISTS `ci_admin_menu` (
  `id` smallint(6) unsigned NOT NULL AUTO_INCREMENT COMMENT '自增ID',
  `pid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '父ID',
  `title` varchar(50) NOT NULL DEFAULT '' COMMENT '标题',
  `url` char(50) NOT NULL DEFAULT '' COMMENT '链接地址',
  `group_name` varchar(50) NOT NULL DEFAULT '' COMMENT '分组名称',
  `description` varchar(255) NOT NULL DEFAULT '' COMMENT '描述',
  `is_hide` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否隐藏（0否 | 1是）',
  `is_founder` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '仅创始人可见',
  `order` smallint(6) NOT NULL DEFAULT '0' COMMENT '排序（同级有效）',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '状态',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COMMENT='后台菜单表';

-- Dumping data for table ci_auth.ci_admin_menu: ~8 rows (approximately)
/*!40000 ALTER TABLE `ci_admin_menu` DISABLE KEYS */;
INSERT INTO `ci_admin_menu` (`id`, `pid`, `title`, `url`, `group_name`, `description`, `is_hide`, `is_founder`, `order`, `status`) VALUES
	(1, 0, '首页', 'index', '', '顶部菜单', 0, 0, 0, 1),
	(2, 0, '全局', 'config', '', '顶部菜单', 0, 0, 0, 1),
	(3, 0, '用户', 'user', '', '顶部菜单', 0, 0, 0, 1),
	(4, 0, '内容', 'article', '', '顶部菜单', 0, 0, 0, 1),
	(5, 0, '招标', 'bidding', '', '顶部菜单', 0, 0, 0, 1),
	(6, 0, '店铺', 'shop', '', '顶部菜单', 0, 0, 0, 1),
	(7, 0, '应用', 'app', '', '顶部菜单', 0, 0, 0, 1),
	(8, 0, '工具', 'tool', '', '顶部菜单', 0, 0, 0, 1);
/*!40000 ALTER TABLE `ci_admin_menu` ENABLE KEYS */;


-- Dumping structure for table ci_auth.ci_admin_role
DROP TABLE IF EXISTS `ci_admin_role`;
CREATE TABLE IF NOT EXISTS `ci_admin_role` (
  `id` smallint(6) unsigned NOT NULL AUTO_INCREMENT COMMENT '角色ID',
  `name` varchar(15) NOT NULL DEFAULT '' COMMENT '角色名称',
  `perms` text COMMENT '拥有的权限点（Controller/Method）',
  `created_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `updated_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '最后修改时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='后台用户角色表';

-- Dumping data for table ci_auth.ci_admin_role: ~0 rows (approximately)
/*!40000 ALTER TABLE `ci_admin_role` DISABLE KEYS */;
/*!40000 ALTER TABLE `ci_admin_role` ENABLE KEYS */;


-- Dumping structure for table ci_auth.ci_admin_user
DROP TABLE IF EXISTS `ci_admin_user`;
CREATE TABLE IF NOT EXISTS `ci_admin_user` (
  `uid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '用户ID',
  `username` varchar(15) NOT NULL DEFAULT '' COMMENT '用户名',
  `roles` varchar(255) NOT NULL DEFAULT '' COMMENT '拥有的角色id',
  `created_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `updated_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '最后更新时间',
  PRIMARY KEY (`uid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='后台用户表';

-- Dumping data for table ci_auth.ci_admin_user: 0 rows
/*!40000 ALTER TABLE `ci_admin_user` DISABLE KEYS */;
/*!40000 ALTER TABLE `ci_admin_user` ENABLE KEYS */;


-- Dumping structure for table ci_auth.ci_auth_groups
DROP TABLE IF EXISTS `ci_auth_groups`;
CREATE TABLE IF NOT EXISTS `ci_auth_groups` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `description` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- Dumping data for table ci_auth.ci_auth_groups: ~2 rows (approximately)
/*!40000 ALTER TABLE `ci_auth_groups` DISABLE KEYS */;
INSERT INTO `ci_auth_groups` (`id`, `name`, `description`) VALUES
	(1, 'admin', 'Administrator'),
	(2, 'members', 'General User');
/*!40000 ALTER TABLE `ci_auth_groups` ENABLE KEYS */;


-- Dumping structure for table ci_auth.ci_auth_login_attempts
DROP TABLE IF EXISTS `ci_auth_login_attempts`;
CREATE TABLE IF NOT EXISTS `ci_auth_login_attempts` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `ip_address` varchar(15) NOT NULL,
  `login` varchar(100) NOT NULL,
  `time` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table ci_auth.ci_auth_login_attempts: ~0 rows (approximately)
/*!40000 ALTER TABLE `ci_auth_login_attempts` DISABLE KEYS */;
/*!40000 ALTER TABLE `ci_auth_login_attempts` ENABLE KEYS */;


-- Dumping structure for table ci_auth.ci_auth_users
DROP TABLE IF EXISTS `ci_auth_users`;
CREATE TABLE IF NOT EXISTS `ci_auth_users` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `ip_address` varchar(15) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(255) NOT NULL,
  `salt` varchar(255) DEFAULT NULL,
  `email` varchar(100) NOT NULL,
  `activation_code` varchar(40) DEFAULT NULL,
  `forgotten_password_code` varchar(40) DEFAULT NULL,
  `forgotten_password_time` int(11) unsigned DEFAULT NULL,
  `remember_code` varchar(40) DEFAULT NULL,
  `created_on` int(11) unsigned NOT NULL,
  `last_login` int(11) unsigned DEFAULT NULL,
  `active` tinyint(1) unsigned DEFAULT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `company` varchar(100) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- Dumping data for table ci_auth.ci_auth_users: ~1 rows (approximately)
/*!40000 ALTER TABLE `ci_auth_users` DISABLE KEYS */;
INSERT INTO `ci_auth_users` (`id`, `ip_address`, `username`, `password`, `salt`, `email`, `activation_code`, `forgotten_password_code`, `forgotten_password_time`, `remember_code`, `created_on`, `last_login`, `active`, `first_name`, `last_name`, `company`, `phone`) VALUES
	(1, '127.0.0.1', 'administrator', '$2a$07$SeBknntpZror9uyftVopmu61qg0ms8Qv1yV6FG.kQOSM.9QhmTo36', '', 'admin@admin.com', '', NULL, NULL, '24NP/q9ty3Rd1ZTEAd5oYe', 1268889823, 1415863302, 1, 'Admin', 'istrator', 'ADMIN', '0');
/*!40000 ALTER TABLE `ci_auth_users` ENABLE KEYS */;


-- Dumping structure for table ci_auth.ci_auth_users_groups
DROP TABLE IF EXISTS `ci_auth_users_groups`;
CREATE TABLE IF NOT EXISTS `ci_auth_users_groups` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned NOT NULL,
  `group_id` mediumint(8) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uc_users_groups` (`user_id`,`group_id`),
  KEY `fk_users_groups_users1_idx` (`user_id`),
  KEY `fk_users_groups_groups1_idx` (`group_id`),
  CONSTRAINT `fk_users_groups_groups1` FOREIGN KEY (`group_id`) REFERENCES `ci_auth_groups` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_users_groups_users1` FOREIGN KEY (`user_id`) REFERENCES `ci_auth_users` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- Dumping data for table ci_auth.ci_auth_users_groups: ~2 rows (approximately)
/*!40000 ALTER TABLE `ci_auth_users_groups` DISABLE KEYS */;
INSERT INTO `ci_auth_users_groups` (`id`, `user_id`, `group_id`) VALUES
	(1, 1, 1),
	(2, 1, 2);
/*!40000 ALTER TABLE `ci_auth_users_groups` ENABLE KEYS */;


-- Dumping structure for table ci_auth.ci_common_setting
DROP TABLE IF EXISTS `ci_common_setting`;
CREATE TABLE IF NOT EXISTS `ci_common_setting` (
  `key` varchar(30) NOT NULL DEFAULT '' COMMENT '配置项',
  `value` text NOT NULL COMMENT '缓存值',
  `type` enum('string','array','object') NOT NULL DEFAULT 'string' COMMENT '配置值类型',
  `description` text NOT NULL COMMENT '配置介绍',
  PRIMARY KEY (`key`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='网站配置表';

-- Dumping data for table ci_auth.ci_common_setting: 1 rows
/*!40000 ALTER TABLE `ci_common_setting` DISABLE KEYS */;
INSERT INTO `ci_common_setting` (`key`, `value`, `type`, `description`) VALUES
	('extsize', 'asd', 'string', '');
/*!40000 ALTER TABLE `ci_common_setting` ENABLE KEYS */;


-- Dumping structure for table ci_auth.ci_user
DROP TABLE IF EXISTS `ci_user`;
CREATE TABLE IF NOT EXISTS `ci_user` (
  `uid` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '用户ID',
  `username` varchar(15) NOT NULL DEFAULT '' COMMENT '用户名称',
  `password` char(32) NOT NULL DEFAULT '' COMMENT '用户密码',
  `salt` char(6) NOT NULL DEFAULT '' COMMENT '盐值（用户密码随机佐料）',
  `email` varchar(100) NOT NULL DEFAULT '' COMMENT 'Email地址',
  `gid` mediumint(8) unsigned NOT NULL DEFAULT '0' COMMENT '当前用户组ID',
  `regdate` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '注册时间',
  `status` smallint(6) unsigned NOT NULL DEFAULT '0' COMMENT '状态',
  PRIMARY KEY (`uid`),
  UNIQUE KEY `idx_username` (`username`),
  KEY `idx_email` (`email`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='用户主表';

-- Dumping data for table ci_auth.ci_user: 2 rows
/*!40000 ALTER TABLE `ci_user` DISABLE KEYS */;
INSERT INTO `ci_user` (`uid`, `username`, `password`, `salt`, `email`, `gid`, `regdate`, `status`) VALUES
	(1, 'admin', '8782b99a5e84e0a195e633bd3c402e16', 'vW4bkD', 'admin@admin.com', 1, 0, 1),
	(2, 'user', 'b6d1daa70f98f5a90a2aba4f28c0d5c4', 'h7oB1y', 'user@admin.com', 2, 0, 1);
/*!40000 ALTER TABLE `ci_user` ENABLE KEYS */;


-- Dumping structure for table ci_auth.ci_user_active_code
DROP TABLE IF EXISTS `ci_user_active_code`;
CREATE TABLE IF NOT EXISTS `ci_user_active_code` (
  `uid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '用户ID',
  `email` varchar(80) NOT NULL DEFAULT '' COMMENT 'Email地址',
  `code` varchar(10) NOT NULL DEFAULT '' COMMENT '激活码',
  `send_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '发送时间',
  `active_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '激活时间',
  `typeid` tinyint(1) NOT NULL DEFAULT '0' COMMENT '类型（1为注册邮箱 | 2为找回密码）',
  PRIMARY KEY (`uid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='用户邮箱激活码表';

-- Dumping data for table ci_auth.ci_user_active_code: 0 rows
/*!40000 ALTER TABLE `ci_user_active_code` DISABLE KEYS */;
/*!40000 ALTER TABLE `ci_user_active_code` ENABLE KEYS */;


-- Dumping structure for table ci_auth.ci_user_data
DROP TABLE IF EXISTS `ci_user_data`;
CREATE TABLE IF NOT EXISTS `ci_user_data` (
  `uid` int(10) unsigned NOT NULL COMMENT '用户ID',
  `last_login_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '最后登陆时间',
  `last_login_ip` varchar(20) NOT NULL DEFAULT '' COMMENT '最后登录IP',
  PRIMARY KEY (`uid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- Dumping data for table ci_auth.ci_user_data: 1 rows
/*!40000 ALTER TABLE `ci_user_data` DISABLE KEYS */;
INSERT INTO `ci_user_data` (`uid`, `last_login_time`, `last_login_ip`) VALUES
	(1, 1415959465, '127.0.0.1');
/*!40000 ALTER TABLE `ci_user_data` ENABLE KEYS */;


-- Dumping structure for table ci_auth.ci_user_group
DROP TABLE IF EXISTS `ci_user_group`;
CREATE TABLE IF NOT EXISTS `ci_user_group` (
  `gid` mediumint(8) unsigned NOT NULL AUTO_INCREMENT COMMENT '用户组ID',
  `name` varchar(16) NOT NULL DEFAULT '' COMMENT '用户组名字',
  `type` enum('default','member','system','special','vip') NOT NULL DEFAULT 'default' COMMENT '用户组类型',
  PRIMARY KEY (`gid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='用户组表';

-- Dumping data for table ci_auth.ci_user_group: 0 rows
/*!40000 ALTER TABLE `ci_user_group` DISABLE KEYS */;
/*!40000 ALTER TABLE `ci_user_group` ENABLE KEYS */;


-- Dumping structure for table ci_auth.ci_user_group_perm
DROP TABLE IF EXISTS `ci_user_group_perm`;
CREATE TABLE IF NOT EXISTS `ci_user_group_perm` (
  `gid` mediumint(8) unsigned NOT NULL DEFAULT '0' COMMENT '用户组ID',
  `rkey` varchar(64) NOT NULL DEFAULT '' COMMENT '权限点',
  `rtype` enum('basic','system') NOT NULL DEFAULT 'basic' COMMENT '权限类型',
  `rvalue` text COMMENT '权限值',
  `vtype` enum('string','array') NOT NULL DEFAULT 'string' COMMENT '权限值类型',
  PRIMARY KEY (`gid`,`rkey`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='用户组前端权限表';

-- Dumping data for table ci_auth.ci_user_group_perm: 0 rows
/*!40000 ALTER TABLE `ci_user_group_perm` DISABLE KEYS */;
/*!40000 ALTER TABLE `ci_user_group_perm` ENABLE KEYS */;


-- Dumping structure for table ci_auth.ci_user_group_relation
DROP TABLE IF EXISTS `ci_user_group_relation`;
CREATE TABLE IF NOT EXISTS `ci_user_group_relation` (
  `uid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '用户ID',
  `gid` mediumint(8) NOT NULL DEFAULT '0' COMMENT '用户组ID',
  `endtime` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '有效期',
  UNIQUE KEY `idx_uid_gid` (`uid`,`gid`),
  KEY `idx_gid` (`gid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='用户与用户组关系表';

-- Dumping data for table ci_auth.ci_user_group_relation: 1 rows
/*!40000 ALTER TABLE `ci_user_group_relation` DISABLE KEYS */;
INSERT INTO `ci_user_group_relation` (`uid`, `gid`, `endtime`) VALUES
	(1, 1, 0);
/*!40000 ALTER TABLE `ci_user_group_relation` ENABLE KEYS */;


-- Dumping structure for table ci_auth.ci_user_login_attempt
DROP TABLE IF EXISTS `ci_user_login_attempt`;
CREATE TABLE IF NOT EXISTS `ci_user_login_attempt` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '自增ID',
  `ip_address` varchar(20) NOT NULL COMMENT 'IP地址',
  `login` varchar(100) NOT NULL COMMENT '登陆的账号（用户名|邮箱|其他）',
  `login_time` int(10) unsigned DEFAULT NULL COMMENT '登陆时间',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 COMMENT='用户尝试登陆记录表';

-- Dumping data for table ci_auth.ci_user_login_attempt: 6 rows
/*!40000 ALTER TABLE `ci_user_login_attempt` DISABLE KEYS */;
INSERT INTO `ci_user_login_attempt` (`id`, `ip_address`, `login`, `login_time`) VALUES
	(9, '127.0.0.1', 'admin@admin.com', 1415959518),
	(10, '127.0.0.1', 'admin@admin.com', 1415959522),
	(11, '127.0.0.1', 'admin@admin.com', 1415959523),
	(12, '127.0.0.1', 'admin@admin.com', 1415959523),
	(13, '127.0.0.1', 'admin@admin.com', 1415959524),
	(14, '127.0.0.1', 'admin@admin.com', 1415959525),
	(15, '127.0.0.1', 'admin@admin.com', 1415959646);
/*!40000 ALTER TABLE `ci_user_login_attempt` ENABLE KEYS */;


-- Dumping structure for table ci_auth.ci_user_sessions
DROP TABLE IF EXISTS `ci_user_sessions`;
CREATE TABLE IF NOT EXISTS `ci_user_sessions` (
  `session_id` varchar(40) NOT NULL DEFAULT '0' COMMENT 'session ID',
  `ip_address` varchar(45) NOT NULL DEFAULT '0' COMMENT 'IP地址',
  `user_agent` varchar(120) NOT NULL COMMENT '用户浏览器信息（取前120个字符）',
  `last_activity` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '最新的一个活跃时间戳',
  `user_data` text NOT NULL COMMENT '用户session数据（serialized）',
  PRIMARY KEY (`session_id`),
  KEY `idx_last_activity` (`last_activity`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='CI session 数据表';

-- Dumping data for table ci_auth.ci_user_sessions: 0 rows
/*!40000 ALTER TABLE `ci_user_sessions` DISABLE KEYS */;
/*!40000 ALTER TABLE `ci_user_sessions` ENABLE KEYS */;


-- Dumping structure for table ci_auth.tk_auth_group
DROP TABLE IF EXISTS `tk_auth_group`;
CREATE TABLE IF NOT EXISTS `tk_auth_group` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `title` char(100) NOT NULL DEFAULT '',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `rules` char(80) NOT NULL DEFAULT '',
  `describe` char(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

-- Dumping data for table ci_auth.tk_auth_group: 8 rows
/*!40000 ALTER TABLE `tk_auth_group` DISABLE KEYS */;
INSERT INTO `tk_auth_group` (`id`, `title`, `status`, `rules`, `describe`) VALUES
	(1, '超级管理员', 1, '', '拥有全部权限'),
	(2, '网站管理员', 1, '11,12,13,14,2,1,7,9,15,16,17', '拥有相对多的权限'),
	(3, '发布人员', 1, '2,15,16,17', '拥有发布、修改文章的权限'),
	(4, '编辑', 1, '11,12,13,14,2', '拥有文章模块的所有权限'),
	(5, '积分小于50', 1, '2,15', '积分小于50'),
	(6, '积分大于50小于200', 1, '2,16', '积分大于50小于200'),
	(7, '积分大于200', 1, '2,17', '积分大于200'),
	(8, '默认组', 1, '2,1,3', '拥有一些通用的权限');
/*!40000 ALTER TABLE `tk_auth_group` ENABLE KEYS */;


-- Dumping structure for table ci_auth.tk_auth_group_access
DROP TABLE IF EXISTS `tk_auth_group_access`;
CREATE TABLE IF NOT EXISTS `tk_auth_group_access` (
  `uid` mediumint(8) unsigned NOT NULL,
  `group_id` mediumint(8) unsigned NOT NULL,
  UNIQUE KEY `uid_group_id` (`uid`,`group_id`),
  KEY `uid` (`uid`),
  KEY `group_id` (`group_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- Dumping data for table ci_auth.tk_auth_group_access: 7 rows
/*!40000 ALTER TABLE `tk_auth_group_access` DISABLE KEYS */;
INSERT INTO `tk_auth_group_access` (`uid`, `group_id`) VALUES
	(1, 1),
	(2, 2),
	(3, 3),
	(4, 4),
	(5, 5),
	(6, 6),
	(7, 7);
/*!40000 ALTER TABLE `tk_auth_group_access` ENABLE KEYS */;


-- Dumping structure for table ci_auth.tk_auth_rule
DROP TABLE IF EXISTS `tk_auth_rule`;
CREATE TABLE IF NOT EXISTS `tk_auth_rule` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `name` char(80) NOT NULL DEFAULT '',
  `title` char(20) NOT NULL DEFAULT '',
  `type` tinyint(1) NOT NULL DEFAULT '1',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `condition` char(100) NOT NULL DEFAULT '',
  `mid` tinyint(3) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=MyISAM AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;

-- Dumping data for table ci_auth.tk_auth_rule: 17 rows
/*!40000 ALTER TABLE `tk_auth_rule` DISABLE KEYS */;
INSERT INTO `tk_auth_rule` (`id`, `name`, `title`, `type`, `status`, `condition`, `mid`) VALUES
	(1, 'Admin/Auth/accessList', '权限列表', 1, 1, '', 3),
	(2, 'Admin/Index/index', '后台首页', 1, 1, '', 2),
	(3, 'Admin/Auth/accessAdd', '添加权限页面', 1, 1, '', 3),
	(4, 'Admin/Auth/groupList', '角色管理页面', 1, 1, '', 3),
	(5, 'Admin/Auth/addHandle', '添加权限', 1, 1, '', 3),
	(6, 'Admin/Auth/groupAddHandle', '添加角色', 1, 1, '', 3),
	(7, 'Admin/Auth/accessSelect', '角色授权页面', 1, 1, '', 3),
	(8, 'Admin/Auth/accessSelectHandle', '更新角色权限', 1, 1, '', 3),
	(9, 'Admin/Auth/groupMember', '角色组成员列表', 1, 1, '', 3),
	(10, 'Admin/Auth/accessDelHandle', '删除权限', 1, 1, '', 3),
	(11, 'Admin/Member/memberList', '会员列表', 1, 1, '', 1),
	(12, 'Admin/Member/memberAdd', '添加会员页面', 1, 1, '', 1),
	(13, 'Admin/Member/addHandle', '添加会员', 1, 1, '', 1),
	(14, 'Admin/Member/deleteHandle', '删除会员', 1, 1, '', 1),
	(15, 'score50', '积分小于50', 1, 1, '{score}<50', 4),
	(16, 'score100', '积分大于50小于200', 1, 1, '{score}>50 and {score}<200', 4),
	(17, 'score200', '积分大于200', 1, 1, '{score}>200', 4);
/*!40000 ALTER TABLE `tk_auth_rule` ENABLE KEYS */;


-- Dumping structure for table ci_auth.tk_members
DROP TABLE IF EXISTS `tk_members`;
CREATE TABLE IF NOT EXISTS `tk_members` (
  `uid` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(20) NOT NULL DEFAULT '',
  `password` char(32) NOT NULL DEFAULT '',
  `score` mediumint(8) unsigned NOT NULL,
  PRIMARY KEY (`uid`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- Dumping data for table ci_auth.tk_members: 7 rows
/*!40000 ALTER TABLE `tk_members` DISABLE KEYS */;
INSERT INTO `tk_members` (`uid`, `username`, `password`, `score`) VALUES
	(1, 'admin', '21232f297a57a5a743894a0e4a801fc3', 500),
	(2, 'manager', '21232f297a57a5a743894a0e4a801fc3', 150),
	(3, 'publish', '21232f297a57a5a743894a0e4a801fc3', 500),
	(4, 'edit', '21232f297a57a5a743894a0e4a801fc3', 5000),
	(5, 'score50', '21232f297a57a5a743894a0e4a801fc3', 30),
	(6, 'score100', '21232f297a57a5a743894a0e4a801fc3', 150),
	(7, 'score200', '21232f297a57a5a743894a0e4a801fc3', 300);
/*!40000 ALTER TABLE `tk_members` ENABLE KEYS */;


-- Dumping structure for table ci_auth.tk_modules
DROP TABLE IF EXISTS `tk_modules`;
CREATE TABLE IF NOT EXISTS `tk_modules` (
  `id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `moduleName` varchar(20) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- Dumping data for table ci_auth.tk_modules: ~4 rows (approximately)
/*!40000 ALTER TABLE `tk_modules` DISABLE KEYS */;
INSERT INTO `tk_modules` (`id`, `moduleName`) VALUES
	(1, '会员管理'),
	(2, '后台管理'),
	(3, '权限管理'),
	(4, '其他');
/*!40000 ALTER TABLE `tk_modules` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
