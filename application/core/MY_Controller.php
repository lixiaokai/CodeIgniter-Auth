<?php if (! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * 扩展CodeIgniter控制器类
 */

/**
 * 基础控制器
 * 前端后端控制都继承该控制器类
 */
class Base_Controller extends CI_Controller {

    public function __construct() {
        parent::__construct();

        header('Content-Type: text/html; charset=UTF-8');

        //定义公共常量
        define('VERSION', $this->config->item('version'));
        define('DIRECTORY_NAME', $this->router->fetch_directory());
        define('CLASS_NAME', $this->router->fetch_class());
        define('METHOD_NAME', $this->router->fetch_method());
        define('TOKEN', $this->security->get_csrf_hash());
        define('TIMESTAMP', $_SERVER['REQUEST_TIME']);
        define('IS_POST', $_SERVER['REQUEST_METHOD'] === 'POST' && count($_POST) ? true : false);

        log_message('debug', "Base_Controller Controller Class Initialized");
    }
}

/**
 * 前端控制器
 * 前端的所有控制器都需要继承这个类，它不包含验证
 */
class Front_Controller extends Base_Controller {

    public function __construct() {
        parent::__construct();

        //定义前端常量
        //...

        //模板路径
        define('VIEW_PATH', 'front/' . CLASS_NAME . '/' . METHOD_NAME);

        log_message('debug', "Front_Controller Controller Class Initialized");
    }
}

/**
 * 后端控制器
 * 后端的所有控制器都需要继承这个类，主要包含验证
 */
class Admin_Controller extends Base_Controller {

    public function __construct() {
        parent::__construct();

        //开发模式开启调试信息
        if (defined('ENVIRONMENT') && ENVIRONMENT == 'development') $this->output->enable_profiler(true);

        //定义前端常量
        //...

        //模板路径
        define('VIEW_PATH', 'admin/' . CLASS_NAME . '/' . METHOD_NAME);

        log_message('debug', "Admin_Controller Controller Class Initialized");
    }
}

/* End of file MY_Controller.php */
/* Location: ./application/core/MY_Controller.php */
