<?php if (! defined('BASEPATH')) exit('No direct access allowed.');

/**
 * 业务服务类
 * 实现业务逻辑，所有业务服务都必须继承该类
 *
 * CI框架默认只有模型(M)-视图(V)-控制器(C)
 * 那么业务逻辑写在哪一层好呢？
 * 于是扩展了核心类，把业务逻辑写在这里
 */

class MY_Service {

    protected $_msgs = array(); //一般消息
    protected $_errs = array(); //错误消息

    public function __construct() {
        log_message('debug', "MY_Service Class Initialized");
    }

    /**
     * 使得CI超级全局对象可以直接使用
     *
     * @param	$key
     * @return	mixed
     */
    public function __get($key) {
        $CI = &get_instance();
        return $CI->$key;
    }


    /**
     * 设置错误信息
     *
     * @return void
     */
    public function set_message($msg) {
        $this->_msgs[] = $msg;
        return $msg;
    }

    /**
     * 获取错误信息
     *
     * @return void
     */
    public function messages() {
        $output = '';
        foreach ($this->_msgs as $msg) {
            $output .= $this->lang->line($msg) ? $this->lang->line($msg) : '##' . $msg . '##';
        }
        return $output;
    }

    /**
     * 获取错误消息（返回数组）
     *
     * @param bool $langify 是否读取语言配置
     * @return array
     */
    public function messages_array($langify = true) {
        if (! $langify) return $this->_msgs;

        $output = array();
        foreach ($this->_msgs as $msg) {
            $output[] = $this->lang->line($msg) ? $this->lang->line($msg) : '##' . $msg . '##';
        }
        return $output;
    }

    /**
     * 设置错误信息
     *
     * @return void
     */
    public function set_error($err) {
        $this->_errs[] = $err;
        return $err;
    }

    /**
     * 获取错误信息
     *
     * @return void
     */
    public function errors() {
        $output = '';
        foreach ($this->_errs as $err) {
            $output .= $this->lang->line($err) ? $this->lang->line($err) : '##' . $err . '##';
        }
        return $output;
    }

    /**
     * 获取错误消息（返回数组）
     *
     * @param bool $langify 是否读取语言配置
     * @return array
     */
    public function errors_array($langify = true) {
        if (! $langify) return $this->_errs;

        $output = array();
        foreach ($this->_errs as $err) {
            $output[] = $this->lang->line($err) ? $this->lang->line($err) : '##' . $err . '##';
        }
        return $output;
    }
}

/* End of file My_Service.php */
/* Location: ./application/core/My_Service.php */
