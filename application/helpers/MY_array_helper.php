<?php if (! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * 扩展数组辅助函数
 */

/**
 * 返回数组中指定的一列
 * http://www.onethink.cn
 * /Application/Common/Common/function.php
 *
 * array_column — PHP 5 >= 5.5.0 默认函数
 * http://www.php.net/manual/zh/function.array-column.php
 * PHP 5 < 5.5.0 则使用自定义函数
 *
 * @access public
 * @param array $input 需要取出数组列的多维数组（或结果集）
 * @param string $columnKey 需要返回值的列，它可以是索引数组的列索引，或者是关联数组的列的键。也可以是NULL，此时将返回整个数组（配合indexKey参数来重置数组键的时候，非常管用）
 * @param string $indexKey 作为返回数组的索引/键的列，它可以是该列的整数索引，或者字符串键值。
 * @return array
 */
if (! function_exists('array_column')) {
    function array_column(array $input, $columnKey, $indexKey = null) {
        $result = array();
        if (null === $indexKey) {
            if (null === $columnKey) {
                $result = array_values($input);
            } else {
                foreach ($input as $row) {
                    $result[] = $row[$columnKey];
                }
            }
        } else {
            if (null === $columnKey) {
                foreach ($input as $row) {
                    $result[$row[$indexKey]] = $row;
                }
            } else {
                foreach ($input as $row) {
                    $result[$row[$indexKey]] = $row[$columnKey];
                }
            }
        }
        return $result;
    }
}

/**
 * **************************************************************************************
 * 数组工具类
 *
 * @author Qiong Wu <papa0924@gmail.com>
 * @copyright ©2003-2103 phpwind.com
 * @license http://www.windframework.com
 * @version $Id: WindArray.php 2973 2011-10-15 19:22:48Z yishuo $
 * @package utility
 * **************************************************************************************
 */

/**
 * 按指定key合并两个数组
 *
 * @param string key    合并数组的参照值
 * @param array $array1  要合并数组
 * @param array $array2  要合并数组
 * @return array 返回合并的数组
 */
function merge_array_with_key($key, array $array1, array $array2) {
    if (! $key || ! $array1 || ! $array2) {
        return array();
    }
    $array1 = rebuild_array_with_key($key, $array1);
    $array2 = rebuild_array_with_key($key, $array2);
    $tmp = array();
    foreach ($array1 as $key => $array) {
        if (isset($array2[$key])) {
            $tmp[$key] = array_merge($array, $array2[$key]);
            unset($array2[$key]);
        } else {
            $tmp[$key] = $array;
        }
    }
    return array_merge($tmp, (array )$array2);
}

/**
 * 按指定key合并两个数组
 *
 * @param string key    合并数组的参照值
 * @param array $array1  要合并数组
 * @param array $array2  要合并数组
 * @return array 返回合并的数组
 */
function filter_array_with_key($key, array $array1, array $array2) {
    if (! $key || ! $array1 || ! $array2) {
        return array();
    }
    $array1 = rebuild_array_with_key($key, $array1);
    $array2 = rebuild_array_with_key($key, $array2);
    $tmp = array();
    foreach ($array1 as $key => $array) {
        if (isset($array2[$key])) {
            $tmp[$key] = array_merge($array, $array2[$key]);
        }
    }
    return $tmp;
}

/**
 * 按指定KEY重新生成数组
 *
 * @param string key 	重新生成数组的参照值
 * @param array  $array 要重新生成的数组
 * @return array 返回重新生成后的数组
 */
function rebuild_array_with_key($key, array $array) {
    if (! $key || ! $array) {
        return array();
    }
    $tmp = array();
    foreach ($array as $_array) {
        if (isset($_array[$key])) {
            $tmp[$_array[$key]] = $_array;
        }
    }
    return $tmp;
}

/* End of file array_helper.php */
/* Location: ./application/helpers/array_helper.php */
