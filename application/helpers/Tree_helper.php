<?php if (! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * 分类树辅助函数
 */

/**
 * 把返回的数据集转换成Tree
 * 巧妙使用php引用实现无限分类
 *
 * @param array     $list   要转换的数据集
 * @param string    $pk     id标记字段
 * @param string    $pid    父id标记字段
 * @param string    $_child 子节点标记字段
 * @param int       $root   要获取数的根节点数值
 *
 * @return array
 * @author 麦当苗儿 <zuojiazi@vip.qq.com>
 */
function list_to_tree($list, $pk = 'id', $pid = 'pid', $_child = '_child', $root = 0) {
    // 创建Tree
    $tree = array();
    if (is_array($list)) {
        // 创建基于主键的数组引用
        $refer = array();
        foreach ($list as $key => $data) {
            $refer[$data[$pk]] = &$list[$key];
        }
        foreach ($list as $key => $data) {
            // 判断是否存在parent
            $parentId = $data[$pid];
            if ($root == $parentId) {
                $tree[$data[$pk]] = &$list[$key];
            } else {
                if (isset($refer[$parentId])) {
                    $parent = &$refer[$parentId];
                    $parent[$_child][$data[$pk]] = &$list[$key];
                }
            }
        }
    }
    return $tree;
}

/**
 * 将list_to_tree的树还原成列表
 *
 * @param  array    $tree       原来的树
 * @param  string   $pk         id标记字段
 * @param  bool     $add_child  列表数组是否附加子节点
 * @param  string   $_child     孩子节点的键
 * @param  int      $depth      节点深度 （从0开始，用于输出节点时缩进）
 * @param  array    &$list      过渡用的中间数组，
 *
 * @return array                返回排过序的列表数组
 * @author yangweijie <yangweijiester@gmail.com>
 */
function tree_to_list($tree, $pk = 'id', $add_child = false, $_child = '_child', $_depth = 0, &$list = array()) {
    if (is_array($tree)) {
        foreach ($tree as $key => $val) {
            $refer = $val;
            $refer['_depth'] = $_depth;
            if (isset($refer[$_child])) {
                if (! $add_child) unset($refer[$_child]);
                $list[$val[$pk]] = $refer;
                tree_to_list($val[$_child], $pk, $add_child, $_child, $_depth + 1, $list);
            } else {
                $list[$val[$pk]] = $refer;
            }
        }
    }
    return $list;
}

/* End of file Tree_helper.php */
/* Location: ./helpers/Tree_helper.php */
