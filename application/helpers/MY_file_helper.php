<?php if (! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * 扩展文件辅助函数
 */

/**
 * 根据mime获取文件后缀
 *
 * 将文件后缀所代表的文件类型按照config/mimes.php里所指明的对应类型进行解释。
 * 如果不能决定文件类型，或者不能打开mime配置文件则返回 FALSE。
 *
 * @access	public
 * @param	string $mime
 * @return	mixed
 */

function get_extension_by_mime($mime) {
    global $mimes;

    $type = array('jpeg' => 'jpg');

    if (! is_array($mimes)) {
        if (defined('ENVIRONMENT') and is_file(APPPATH . 'config/' . ENVIRONMENT . '/mimes.php')) {
            include (APPPATH . 'config/' . ENVIRONMENT . '/mimes.php');
        } elseif (is_file(APPPATH . 'config/mimes.php')) {
            include (APPPATH . 'config/mimes.php');
        }

        if (! is_array($mimes)) {
            return false;
        }
    }

    foreach ($mimes as $ext => $_mime) {
        if (is_array($_mime)) {
            foreach ($_mime as $__mime) {
                if ($mime == $__mime) return isset($type[$ext]) ? $type[$ext] : $ext;
            }
        } else {
            if ($mime == $_mime) return isset($type[$ext]) ? $type[$ext] : $ext;
        }
    }

    return false;
}

/* End of file MY_file_helper.php */
/* Location: ./helpers/MY_file_helper.php */
