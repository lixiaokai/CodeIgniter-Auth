<?php if (! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * 扩展表单辅助函数
 */

/**
 * 返回html checked
 *
 * @param boolean $var
 * @return string
 */
function if_check($var) {
    return $var ? ' checked' : '';
}

/**
 * 返回html selected
 *
 * @param boolean $var
 * @return string
 */
function if_select($var) {
    return $var ? ' selected style="background-color:#DDD;"' : '';
}

/**
 * 返回html active
 *
 * @param boolean $var
 * @return string
 */
function if_active($var) {
    return $var ? ' active' : '';
}

/* End of file MY_Form_helper.php */
/* Location: ./helpers/MY_Form_helper.php */
