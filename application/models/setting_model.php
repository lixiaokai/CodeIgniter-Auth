<?php if (! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * 网站配置 模型
 */

class Setting_model extends MY_Model {

    protected $_table = 'common_setting';
    protected $primary_key = 'key';
    protected $return_type = 'array';

    public function __construct() {
        parent::__construct();
    }

    /**
     * 新增或更新配置项
     *
     * @param	string	$key
     * @param	string	$value
     * @param   string  $description 配置项描述
     * @return	bool
     */
    public function save_setting($key, $value, $description = null) {
        $save = array();
        list($save['type'], $save['value']) = $this->_to_string($value);
        isset($description) && $save['description'] = $description;
        if ($this->get($key)) {
            $this->update($key, $save);
        } else {
            $save['key'] = $key;
            $this->insert($save);
        }
        return $this->db->affected_rows() > -1;
    }

    /**
     * 将数据转换为字符串
     *
     * @param mixed $value 待处理的数据
     * @return array 返回处理后的数据，第一个代表该数据的类型，第二个代表该数据处理后的数据串
     */
    private function _to_string($value) {
        $vtype = 'string';
        if (is_array($value)) {
            $value = serialize($value);
            $vtype = 'array';
        } elseif (is_object($value)) {
            $value = serialize($value);
            $vtype = 'object';
        }
        return array($vtype, $value);
    }
}

/* End of file setting_model.php */
/* Location: ./application/models/setting_model.php */
