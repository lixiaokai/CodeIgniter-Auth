<?php if (! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * 后台用户 模型
 */

class Admin_user_model extends MY_Model {

    protected $_table = 'admin_user';
    protected $primary_key = 'uid';
    protected $return_type = 'array';

    public function __construct() {
        parent::__construct();
        
        log_message('debug', "Admin_user_model Model Class Initialized");
    }
}

/* End of file admin_user_model.php */
/* Location: ./application/models/admin_user_model.php */
