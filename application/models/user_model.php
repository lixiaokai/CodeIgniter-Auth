<?php if (! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * 用户 模型
 */

class User_model extends MY_Model {

    protected $_table = 'user';
    protected $primary_key = 'uid';
    protected $return_type = 'array';

    public function __construct() {
        parent::__construct();
    }
}

/* End of file user_model.php */
/* Location: ./application/models/user_model.php */
