<?php

if (! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * 后台用户验证配置
 */

/**
 ***************************************************************************
 * 创始人uids
 ***************************************************************************
 * 
 * array(1,2,3) 表示uid等于1,2,3的用户为创始人
 *
 */
$config['founder_uids'] = array(1);

/* End of file admin_auth.php */
/* Location: ./application/config/admin_auth.php */
