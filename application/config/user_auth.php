<?php

/**
 * 用户认证配置参数
 *
 */


/*
| -------------------------------------------------------------------------
| tables 数据表
| -------------------------------------------------------------------------
| user              用户表
| group             用户组表
| user_group        用户与用户组关系表
| login_attempt     用户登陆尝试记录表
|
| -------------------------------------------------------------------------
| join 关联字段
| -------------------------------------------------------------------------
| user              用户id关联字段
| group             用户组id关联字段
*/
$config['tables']['user'] = 'user';
$config['tables']['group'] = 'user_group';
$config['tables']['user_group'] = 'user_group_relation';
$config['tables']['login_attempt'] = 'login_attempt';

$config['join']['user'] = 'uid';
$config['join']['group'] = 'gid';


/*
| -------------------------------------------------------------------------
| options 选项
| -------------------------------------------------------------------------
| allow_register		允许新用户注册（0关闭注册 | 1开放注册 | 2邀请注册）
| protocol				注册协议内容。支持html代码
| security_ip			同一IP重复注册[小时]。 规定时间内，同一IP将无法进行多次注册。0或留空表示不限制
| manual_active			新用户注册审核。开启后新注册用户需要管理员后台手动激活才能拥有正常的权限
| email_active			新用户邮件激活。开启后，系统将向注册 电子邮箱 发送一封验证电子邮件以确认电子邮箱的有效性。
|						用户收到电子邮件并激活帐号后才能拥有正常的权限
| ban_username			禁用用户名，多个词之间用英文半角逗号","分隔。包含设定词汇的所有用户名将无法成功注册。
|						如你禁用了"版主"，那么所有含有"版主"(如:我是版主)的用户名将被禁止使用。
| min_username_length	用户名最小长度（Default: 3）
| max_username_length	用户名最大长度（Default: 15）
| min_password_length	密码最小长度（Default: 8）
| max_password_length	密码最大长度（Default: 20）
| password_contain		密码复杂度。小写字母 大写字母 数字 符号 密码不能与用户名相同
| 
| max_login_attempt		用户最大登陆尝试次数
| lockout_time			超过尝试登陆次数后锁定多少秒后才能继续尝试
| 
| reset_password_expiration 重设密码有效期
| 
| 
*/
$config['allow_register'] = 1;
$config['protocol'] = '';

$config['admin_email'] = "admin@example.com"; // 管理员邮箱
$config['default_group'] = 'members'; // 默认用户组
$config['admin_group'] = 'admin'; // 默认管理组
$config['identity'] = 'email'; // 默认登陆方式
$config['min_password_length'] = 8; // 密码最小长度
$config['max_password_length'] = 20; // 密码最大长度
$config['email_activation'] = false; // 注册时是否需要邮箱激活 (Default: false)
$config['manual_activation'] = false; // 注册时是否手动激活(Default: false)
$config['remember_users'] = true; // 让用户记住并启用自动登录
$config['user_expire'] = 86500; // 记住用户过期时间，设置为0表示无限期
$config['user_extend_on_login'] = false; // 每次自动登陆时延长用户过期时间
$config['track_login_attempts'] = false; // 跟踪用户失败登陆信息
$config['track_login_ip_address'] = true; // 根据IP来跟踪用户失败登陆信息，如果设置为false，则使用用户账号来跟踪 (Default: true)
$config['maximum_login_attempts'] = 3; // 最大失败登陆尝试次数
$config['lockout_time'] = 600; // 超过尝试登陆次数后锁定多少秒后才能继续尝试
$config['forgot_password_expiration'] = 0; // 重试密码过期时间(毫秒)