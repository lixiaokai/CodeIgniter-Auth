<?php if (! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * 用户认证配置参数
 * 基于 Ion Auth V2.5.2 修改而来
 *
 * Author: Ben Edmunds
 *		   ben.edmunds@gmail.com
 *         @benedmunds
 * Location: http://github.com/benedmunds/CodeIgniter-Ion-Auth
 */

/*
| -------------------------------------------------------------------------
| tables 数据表
| -------------------------------------------------------------------------
| user              用户表
| group             用户组表
| user_group        用户与用户组关系表
| login_attempt     用户登陆尝试记录表
|
| -------------------------------------------------------------------------
| join 关联字段
| -------------------------------------------------------------------------
| user              用户id关联字段
| group             用户组id关联字段
*/
$config['tables']['user'] = 'user';
$config['tables']['group'] = 'user_group';
$config['tables']['user_group'] = 'user_group_relation';
$config['tables']['login_attempt'] = 'login_attempt';

$config['join']['user'] = 'uid';
$config['join']['group'] = 'gid';

/*
| -------------------------------------------------------------------------
| 加密方法 (sha1 or bcrypt)
| -------------------------------------------------------------------------
| Bcrypt 在 PHP 5.3+ 默认支持
|
| 重点: 专家们强烈建议使用 bcrypt 代替 sha1
|
| 注意: 如果使用 bcrypt ，密码字段的长度至少要80个字符
|
| Below there is "default_rounds" setting.  This defines how strong the encryption will be,
| but remember the more rounds you set the longer it will take to hash (CPU usage) So adjust
| this based on your server hardware.
|
| If you are using Bcrypt the Admin password field also needs to be changed in order login as admin:
| $2a$07$SeBknntpZror9uyftVopmu61qg0ms8Qv1yV6FG.kQOSM.9QhmTo36
|
| Be careful how high you set max_rounds, I would do your own testing on how long it takes
| to encrypt with x rounds.
|
| salt_prefix: Used for bcrypt. Versions of PHP before 5.3.7 only support "$2a$" as the salt prefix
| Versions 5.3.7 or greater should use the default of "$2y$".
*/
$config['hash_method'] = 'bcrypt'; // sha1 or bcrypt, 强烈推荐使用 bcrypt
$config['default_rounds'] = 8; // 如果random_rounds设置为true,该选项不生效
$config['random_rounds'] = false;
$config['min_rounds'] = 5;
$config['max_rounds'] = 9;
$config['salt_prefix'] = '$2y$';

/*
| -------------------------------------------------------------------------
| Authentication options.
| -------------------------------------------------------------------------
| maximum_login_attempts: This maximum is not enforced by the library, but is
| used by $this->ion_auth->is_max_login_attempts_exceeded().
| The controller should check this function and act
| appropriately. If this variable set to 0, there is no maximum.
*/
$config['site_title'] = "Example.com"; // 站点名称
$config['admin_email'] = "admin@example.com"; // 管理员邮箱
$config['default_group'] = 'members'; // 默认用户组
$config['admin_group'] = 'admin'; // 默认管理组
$config['identity'] = 'email'; // 默认登陆方式
$config['min_password_length'] = 8; // 密码最小长度
$config['max_password_length'] = 20; // 密码最大长度
$config['email_activation'] = false; // 注册时是否需要邮箱激活 (Default: false)
$config['manual_activation'] = false; // 注册时是否手动激活(Default: false)
$config['remember_users'] = true; // 让用户记住并启用自动登录
$config['user_expire'] = 86500; // 记住用户过期时间，设置为0表示无限期
$config['user_extend_on_login'] = false; // 每次自动登陆时延长用户过期时间
$config['track_login_attempts'] = false; // 跟踪用户失败登陆信息
$config['track_login_ip_address'] = true; // 根据IP来跟踪用户失败登陆信息，如果设置为false，则使用用户账号来跟踪 (Default: true)
$config['maximum_login_attempts'] = 3; // 最大失败登陆尝试次数
$config['lockout_time'] = 600; // 超过尝试登陆次数后锁定多少秒后才能继续尝试
$config['forgot_password_expiration'] = 0; // 重试密码过期时间(毫秒)

/*
| -------------------------------------------------------------------------
| Cookie options.
| -------------------------------------------------------------------------
| remember_cookie_name Default: remember_code
| identity_cookie_name Default: identity
*/
$config['remember_cookie_name'] = 'remember_code';
$config['identity_cookie_name'] = 'identity';

/*
| -------------------------------------------------------------------------
| Email options.
| -------------------------------------------------------------------------
| email_config:
| 	  'file' = Use the default CI config or use from a config file
| 	  array  = Manually set your email config settings
*/
$config['use_ci_email'] = false; // Send Email using the builtin CI email class, if false it will return the code and the identity
$config['email_config'] = array('mailtype' => 'html', );

/*
| -------------------------------------------------------------------------
| Email templates.
| -------------------------------------------------------------------------
| Folder where email templates are stored.
| Default: auth/
*/
$config['email_templates'] = 'auth/email/';

/*
| -------------------------------------------------------------------------
| Activate Account Email Template
| -------------------------------------------------------------------------
| Default: activate.tpl.php
*/
$config['email_activate'] = 'activate.tpl.php';

/*
| -------------------------------------------------------------------------
| Forgot Password Email Template
| -------------------------------------------------------------------------
| Default: forgot_password.tpl.php
*/
$config['email_forgot_password'] = 'forgot_password.tpl.php';

/*
| -------------------------------------------------------------------------
| Forgot Password Complete Email Template
| -------------------------------------------------------------------------
| Default: new_password.tpl.php
*/
$config['email_forgot_password_complete'] = 'new_password.tpl.php';

/*
| -------------------------------------------------------------------------
| Salt options
| -------------------------------------------------------------------------
| salt_length Default: 22
|
| store_salt: Should the salt be stored in the database?
| This will change your password encryption algorithm,
| default password, 'password', changes to
| fbaa5e216d163a02ae630ab1a43372635dd374c0 with default salt.
*/
$config['salt_length'] = 22;
$config['store_salt'] = false;

/*
| -------------------------------------------------------------------------
| Message Delimiters.
| -------------------------------------------------------------------------
*/
$config['delimiters_source'] = 'config'; // "config" = use the settings defined here, "form_validation" = use the settings defined in CI's form validation library
$config['message_start_delimiter'] = '<p>'; // Message start delimiter
$config['message_end_delimiter'] = '</p>'; // Message end delimiter
$config['error_start_delimiter'] = '<p>'; // Error mesage start delimiter
$config['error_end_delimiter'] = '</p>'; // Error mesage end delimiter

/* End of file ion_auth.php */
/* Location: ./application/config/ion_auth.php */
