<?php if (! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * 站点设置 控制器
 */
class Setting extends Admin_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function index() {
        $this->load->service('setting/setting_service');
        //var_dump($this->setting_service->get('extsize'));
        //$this->setting_service->set('extsize', 'asd');
    }
}

/* End of file setting.php */
/* Location: ./application/controllers/admin/setting.php */
