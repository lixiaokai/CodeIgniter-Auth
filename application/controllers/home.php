<?php if (! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * 前端默认首页 控制器
 */
class Home extends Front_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function index() {
        //$this->load->service('user/user_service');
        //echo $this->user_service->get_user(1);
        $this->load->view(VIEW_PATH);
    }
}

/* End of file home.php */
/* Location: ./application/controllers/home.php */
