<?php if (! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * 控制器
 */
class User extends Front_Controller {

    public function __construct() {
        parent::__construct();

        //开发模式开启调试信息
        if (defined('ENVIRONMENT') && ENVIRONMENT == 'development') $this->output->enable_profiler(true);
    }

    public function index() {
        $this->load->service('user/user_auth_service');
        //echo $this->user_auth_service->salt();
        //echo $this->user_auth_service->hash_password('user', 'h7oB1y');
        if ($uid = $this->user_auth_service->is_login()) {
            echo $this->session->flashdata('message') . '<br>';
            echo 'UID: ' . $uid . ' <a href="' . site_url('user/logout') . '">退出</a>';
        } else {
            echo $this->session->flashdata('message') . '<br>';
            echo '<a href="' . site_url('user/login') . '">登陆</a>';
            echo ' <a href="' . site_url('user/register') . '">注册</a>';
            echo ' <a href="' . site_url('user/register?username=admin') . '">注册[用户名]</a>';
            echo ' <a href="' . site_url('user/register?email=admin@admin.com') . '">注册[Email]</a>';
        }
    }

    public function login() {
        $this->load->service('user/user_auth_service');
        if ($this->user_auth_service->login('admin@admin.com', 'admin')) {
            //var_dump($this->user_auth_service->messages());exit;
            $this->session->set_flashdata('message', $this->user_auth_service->messages());
            redirect('user/index');
        } else {
            $this->session->set_flashdata('message', $this->user_auth_service->errors());
            redirect('user/index');
        }
    }

    public function logout() {
        $this->load->service('user/user_auth_service');
        $this->user_auth_service->logout();
        $this->session->set_flashdata('message', $this->user_auth_service->messages());
        redirect('user/index');
    }

    public function register() {
        $username = $this->input->get('username', true);
        $email = $this->input->get('email', true);

        $data = array();
        $data['username'] = $username ? $username : 'user10';
        $data['email'] = $email ? $email : 'user10@admin.com';

        $this->load->service('user/user_auth_service');
        if ($this->user_auth_service->register($data['username'], '123456', $data['email'])) {
            $this->session->set_flashdata('message', $this->user_auth_service->messages());
            redirect('user/index');
        }
        else
        {
            $this->session->set_flashdata('message', $this->user_auth_service->errors());
            redirect('user/index');
        }
    }
}

/* End of file user.php */
/* Location: ./application/controllers/user.php */
