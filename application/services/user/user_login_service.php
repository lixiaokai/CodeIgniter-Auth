<?php if (! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * 用户登陆服务类
 */

class User_login_service extends MY_Service {

    public function __construct() {

    }


    /**
     * 用户登陆
     *
     * @param string $identity 用户登录的帐号
	 * @param string $password 用户登录的密码
     * @return bool
     */
    public function login($identity, $password, $remember = false) {

    }
}

/* End of file user_login_service.php */
/* Location: ./application/service/user/user_login_service.php */
