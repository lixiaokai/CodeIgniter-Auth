<?php

if (! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * 后台用户验证服务
 */

class Admin_auth_service extends MY_Service {
    
    //配置
    private $_config = array();
    
    public function __construct() {
        parent::__construct();
        
        //载入配置
        $this->load->config('admin_auth', TRUE);
        $this->_config = $this->config->item('admin_auth');
        
        //载入模型
        $this->load->model('admin_user_model');
        
        log_message('debug', "Admin_auth_service Service Class Initialized");
    }

    /**
     * 是否已经登陆
     * 
     * @return boolean
     */
    public function is_login() {
        $admin = $this->session->userdata('admin');
        return $admin['adminid'];
    }

    /**
     * 是否创始人
     * 根据 config/user_auth.php
     * 配置项'founder_uids'判断是否登陆
     * 
     * @return boolean
     */
    public function is_founder($adminid = 0) {
        $adminid = $adminid ? $adminid : $this->is_login();
        return in_array($adminid, config_item('founder_uids'));
    }

    /**
     * 后台用户登录
     *
     * 返回用户对象.参数信息:
     * <code>
     *     $loginInfo: AdminUser
     * </code>
     *
     * @param string $username 用户名
     * @param string $password 密码
     * @return boolean
     */
    public function login($username, $password) {
        if (empty($username) || empty($password)) {
            die('帐号或密码不能为空');
            return false;
        }
        
        $user = $this->admin_user_model->get_by('username', $password);
        
        if(! $user) {
            die('用户不存在');
            return false;
        }
        
    }

    /**
     * 后台用户退出
     *
     * @return boolean
     */
    public function logout() {
        return true;
    }

    /**
     * 更新最后一次登陆信息
     * 
     * @return boolean
     */
    public function update_last_login($adminid) {

    }

    /**
     * 加密密码
     * 
     * @param string $password 密码
     * @param string $salt 盐值
     * @return string
     */
    public function hash_password($password, $salt = false) {
        return md5($salt . $password);
    }

    /**
     * 设置登陆标志
     * 
     * @return boolean
     */
    public function set_login_session($identity) {
        return true;
    }

    /**
     * 是否超过最大尝试登陆次数
     * 
     * @return boolean
     */
    public function is_max_login_attempt($identity) {
        return true;
    }

    /**
     * 增加尝试登陆记录
     */
    public function increase_login_attempt($identity) {

    }

    /**
     * 清空登陆尝试记录
     */
    public function clear_login_attempts($identity, $expire_period = 86400) {

    }

}

/* End of file user_service.php */
/* Location: ./application/service/user/user_service.php */
