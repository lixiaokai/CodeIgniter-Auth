<?php if (! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * 表单验证类 扩展
 * 扩展方法 http://codeigniter.org.cn/user_guide/general/creating_libraries.html
 *
 * @Thanks https://github.com/phpwind/windframework
 */

class MY_Form_validation extends CI_Form_validation {
    //非法字符
    private static $_illegal_char = array(
        "\\",
        '&',
        ' ',
        "'",
        '"',
        '/',
        '*',
        ',',
        '<',
        '>',
        "\r",
        "\t",
        "\n",
        '#',
        '%',
        '?',
        '　');

    /**
     * 构造函数 继承母类的构造函数
     *
     * 如果你需要在类中使用构造函数,你必须在构造函数中显式继承母类构造函数
     * 如果使用了构造函数且母类的构造函数有参数的话，这个继承类也必须加上参数
     */
    public function __construct($rules = array()) {
        parent::__construct($rules);

        log_message('debug', "MY_Form_validation Class Initialized");
    }

    /**
     *
     * 是否含有非法字符
     *
     * @param string $str
     * @return bool 如果没有含有非法字符则返回true，否则返回false
     */
    public static function valid_illegal($str) {
        return str_replace(self::$_illegal_char, '', $str) != $str;
    }

    /**
     * 匹配用户名只能含有中文、数字、大小写字母、'.'、_
     *
     * @param string $str
     * @return bool 如果匹配成果则返回true，否则返回false
     */
    public static function alpha_zh($str) {
        return (! preg_match('/^[\x7f-\xff\dA-Za-z\.\_]+$/', $str)) ? false : true;
    }

    /**
     * 验证是否是电话号码
     *
     * 国际区号-地区号-电话号码的格式（在国际区号前可以有前导0和前导+号），
     * 国际区号支持0-4位
     * 地区号支持0-6位
     * 电话号码支持4到12位
     *
     * @param string $phone 被验证的电话号码
     * @return boolean 如果验证通过则返回true，否则返回false
     */
    public static function is_telephone($phone) {
        return (! preg_match('/^\+?[0\s]*[\d]{0,4}[\-\s]?\d{0,6}[\-\s]?\d{4,12}$/', $phone)) ? false : true;
    }

    /**
     * 验证是否是手机号码
     * 国际区号-手机号码
     *
     * @param string $number 待验证的号码
     * @return boolean 如果验证失败返回false,验证成功返回true
     */
    public static function is_mobilephone($number) {
        return (! preg_match('/^\+?[0\s]*[\d]{0,4}[\-\s]?\d{4,12}$/', $number)) ? false : true;
    }

    /**
     * 验证是否是QQ号码
     *
     * QQ号码必须是以1-9的数字开头，并且长度5-15为的数字串
     *
     * @param string $qq 待验证的qq号码
     * @return boolean 如果验证成功返回true，否则返回false
     */
    public static function is_qq($qq) {
        return (! preg_match('/^[1-9]\d{4,14}$/', $qq)) ? false : true;
    }

    /**
     * 验证是否是邮政编码
     *
     * 邮政编码是4-8个长度的数字串
     *
     * @param string $str 待验证的邮编
     * @return boolean 如果验证成功返回true，否则返回false
     */
    public static function is_zipcode($str) {
        return (! preg_match('/^\d{4,8}$/', $str)) ? false : true;
    }

    /**
     * 验证是否是合法的身份证号
     *
     * @param string $str 待验证的字串
     * @return boolean 如果是合法的身份证号则返回true，否则返回false
     */
    public static function is_idcard($str) {
        return (! preg_match("/^(?:\d{17}[\d|X]|\d{15})$/", $str)) ? false : true;
    }

    /**
     * 验证是否是合法的url
     * 这种验证不通过（http://localhost/）
     *
     * @param string $str 待验证的字串
     * @return boolean 如果是合法的url则返回true，否则返回false
     */
    public static function is_url($str) {
        return (! preg_match('/^(?:http(?:s)?:\/\/(?:[\w-]+\.)+[\w-]+(?:\:\d+)*+(?:\/[\w- .\/?%&=]*)?)$/', $str)) ? false : true;
    }

    /**
     * 验证是否是中文
     *
     * @param string $str 待验证的字串
     * @return boolean 如果是中文则返回true，否则返回false
     */
    public static function is_zh($str) {
        return (! preg_match('/^[\x{4e00}-\x{9fa5}]+$/u', $str)) ? false : true;
    }

    /**
     * 验证是否有客户端脚本
     *
     * @param string $str 被搜索的 字符串
     * @param array $matches 会被搜索的结果,默认为array()
     * @param boolean $ifAll 是否进行全局正则表达式匹配，默认为false即仅进行一次匹配
     * @return boolean 如果匹配成功返回true，否则返回false
     */
    public static function has_script($str, &$matches = array(), $if_all = false) {
        $reg_exp = '/<script(.*?)>([^\x00]*?)<\/script>/';
        return $if_all ? (bool)preg_match_all($reg_exp, $str, $matches) : (bool)preg_match($reg_exp, $str, $matches);
    }

    /**
     * 是否存在
     *
     * @access	public
     * @param	string   $str 验证表单域名称
     * @param	field    $field = table.field 表名.字段名
     * @return	bool     不存在返回false且抛出错误
     */
    public function is_exist($str, $field) {
        list($table, $field) = explode('.', $field);
        $query = $this->CI->db->get_where($table, array($field => $str));

        return (bool)$query->num_rows();
    }
}

/* End of file MY_Form_validation.php */
/* Location: ./application/libraries/MY_Form_validation.php */
