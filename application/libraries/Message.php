<?php if (! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * 消息类
 */

class Message {

    private $_msgs = array();
    private $_errs = array();

    public function __construct() {
        log_message('debug', "Message Class Initialized");
    }

    /**
     * 设置信息
     *
     * @param string $str
     * @param string $type error | message
     * @return void
     */
    public function set($str, $type = 'error') {
        if ($type == 'error') {
            $this->_errs[] = $str;
        } else {
            $this->_msgs[] = $str;
        }
        return $str;
    }

    /**
     * 获取消息（返回字符串）
     *
     * @param string $type error | message
     * @param boolean $langify 是否读取语言配置
     * @return string
     */
    public function gets($type = 'error', $langify = true) {
        $_output = '';
        if ($type == 'error') {
            foreach ($this->_errs as $_err) {
                $_output .= ($langify && $this->lang->line($_err)) ? $this->lang->line($_err) : '##' . $_err . '##';
            }
        } else {
            foreach ($this->_msgs as $_msg) {
                $_output .= ($langify && $this->lang->line($_msg)) ? $this->lang->line($_msg) : '##' . $_msg . '##';
            }
        }
        $this->clear();
        return $_output;
    }

    /**
     * 获取消息（返回数组）
     *
     * @param string $type error | message
     * @param boolean $langify 是否读取语言配置
     * @return array
     */
    public function gets_array($type = 'error', $langify = true) {
        $output = array();
        if ($type == 'error') {
            if (! $langify) return $this->_errs;
            foreach ($this->_errs as $_err) {
                $output[] = ($langify && $this->lang->line($_err)) ? $this->lang->line($_err) : '##' . $_err . '##';
            }
        } else {
            if (! $langify) return $this->_msgs;
            foreach ($this->_msgs as $_msg) {
                $output[] = ($langify && $this->lang->line($_msg)) ? $this->lang->line($_msg) : '##' . $_msg . '##';
            }
        }
        return $output;
    }

    /**
     * 清除消息
     *
     * @return boolean
     */
    public function clear() {
        $this->_errs[] = array();
        $this->_msgs[] = array();
        return true;
    }
}

/* End of file Message.php */
/* Location: ./application/libraries/Message.php */
