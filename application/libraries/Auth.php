<?php if (! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * 登陆退出认证类
 */

class Auth {

    //配置参数
    protected $_config = array();

    public function __construct($params = array()) {
        if (count($params) > 0) $this->initialize($params);

        log_message('debug', "Auth Class Initialized");
    }

    /**
     * 初始化参数
     *
     * @param array $params 参数数组
     * @return void
     */
    public function initialize($params = array()) {
        if (empty($params) or ! is_array($params)) return false;
        $this->_config = $params;
    }

    /**
     * 忘记密码
     *
     * @return mixed  boolian / array
     **/
    public function forgotten_password($identity) {

    }

    /**
     * forgotten_password_complete
     *
     * @return void
     **/
    public function forgotten_password_complete($code) {
    }

    /**
     * forgotten_password_check
     *
     * @return void
     * @author Michael
     **/
    public function forgotten_password_check($code) {

    }

    /**
     * register
     *
     * @return void
     * @author Mathew
     **/
    public function register($username, $password, $email, $additional_data = array(), $group_ids = array())
        //need to test email activation
        {

    }

    /**
     * logout
     *
     * @return void
     * @author Mathew
     **/
    public function logout() {

    }

    /**
     * logged_in
     *
     * @return bool
     * @author Mathew
     **/
    public function logged_in() {

    }

    /**
     * logged_in
     *
     * @return integer
     * @author jrmadsen67
     **/
    public function get_user_id() {

    }


    /**
     * is_admin
     *
     * @return bool
     * @author Ben Edmunds
     **/
    public function is_admin($id = false) {

    }

    /**
     * in_group
     *
     * @param mixed group(s) to check
     * @param bool user id
     * @param bool check if all groups is present, or any of the groups
     *
     * @return bool
     * @author Phil Sturgeon
     **/
    public function in_group($check_group, $id = false, $check_all = false) {

    }
}

/* End of file Auth.php */
/* Location: ./application/libraries/Auth.php */
